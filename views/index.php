<?php
/**
 * @var libs\Views string $title
 * @var libs\Views string $xml
 */
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title; ?></title>

	<link rel="stylesheet" href="/vendor/bower/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/templates/css/soap.css">

</head>
<body>

<div class="container">
	<div class="page-header">
		<h1><?= $title; ?></h1>
	</div>

	<div class="container" id="train_form">
		<form class="form-inline">
			<div class="container">
				<label class="sr-only" for="train">Номер поезда:</label>

				<div class="input-group">
					<div class="input-group-addon">Номер поезда:</div>
					<input type="text" name="train" class="form-control" placeholder="020У" required pattern="[0-9A-Za-zА-Яа-я]{0,5}">
				</div>
			</div>
			<div class="clearfix h15s"></div>

			<div class="container">
				<div class="form-group">
					<label class="sr-only" for="from">Станция отправления:</label>

					<div class="input-group">
						<div class="input-group-addon">Станция отправления:</div>
						<input type="text" name="from" class="form-control" placeholder="Москва" required>
					</div>
				</div>
				<div class="form-group">
					<label class="sr-only" for="to">Станция прибытия:</label>

					<div class="input-group">
						<div class="input-group-addon">Станция прибытия:</div>
						<input type="text" name="to" class="form-control" placeholder="Санктъ-Петербургъ" required>
					</div>
				</div>
			</div>
			<div class="clearfix h15s"></div>

			<div class="container">
				<div class="form-group">
					<label class="sr-only" for="day">Дата:</label>

					<div class="input-group">
						<div class="input-group-addon">Дата:</div>
						<select name="day" class="form-control">
							<?php
								foreach (range(1, 31) as $val) :
									if ($val == (int)date('d', time())) {
										$selected_day = 'selected';
									} else {
										$selected_day = '';
									}
							?>
								<option value="<?= $val; ?>" <?= $selected_day; ?> ><?= sprintf("%02d", $val); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="sr-only" for="month">Месяц:</label>

					<div class="input-group">
						<div class="input-group-addon">Месяц:</div>
						<select name="month" class="form-control">
							<?php
								foreach (\libs\WsdlSearch::getMonth() as $mkey => $mval) :
									if ($mkey == (int)date('m', time())) {
										$selected_month = 'selected';
									} else {
										$selected_month = '';
									}
								?>
								<option value="<?= $mkey; ?>" <?= $selected_month; ?> ><?= $mval; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="clearfix h30s"></div>
			<button type="submit" class="btn btn-success pull-right">Получить</button>
		</form>
	</div>

	<div class="clearfix h40s"></div>

	<div class="container" id="getResult"></div>
	<div class="clearfix h80s"></div>

	<script id="getResultTpl" type="text/x-jquery-tmpl">
		<div class="page-header">
			<h3>Получаем данные:</h3>
		</div>
		<pre>${xml}</pre>
	</script>

	<script id="errorTpl" type="text/x-jquery-tmpl">
		<div class="alert alert-danger alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${error}</strong>
		<div>
	</script>

</div>

<script src="/vendor/bower/jquery/dist/jquery.min.js"></script>
<script src="/vendor/bower/jquery-tmpl/jquery.tmpl.min.js"></script>
<script src="/vendor/bower/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/templates/js/soap_ajax.js"></script>

</body>
</html>