$(document).ready(function() {

	var search_form = $('#train_form');
	var get_result = $('#getResult');

	// ОТправляем и получаем формы JSON
	search_form.find('form').submit(function(e) {
		// Останавливаем отправку формы
		e.preventDefault();
		// Чистим шаблон
		get_result.empty().show();

		// Получаем параметры формы
		var vars = $(this).serialize();

		$.ajax({
			type:     "POST",
			url:      "/index.php",
			data:     vars,
			dataType: 'json',
			success:  function(data) {
				console.log(data);
				$('#getResultTpl').tmpl(data).appendTo('#getResult');
			},
			error:  function(data) {
				var text =  [{ "error" : "Упс, что-то пошло не так!" }];
				$('#errorTpl').tmpl(text).appendTo('#getResult');
			}
		});

	});

});