<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Content-Type: text/html; charset=utf-8');

// сокращаем константу DIRECTORY_SEPARATOR до DS
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
// Директория всего проекта
defined('BASE_PATH') || define('BASE_PATH', realpath(dirname(__FILE__) . DS));
// Директория всего проекта
defined('VIEW') || define('VIEW', BASE_PATH . DS . 'views' . DS);

/** Автозагрузка классов */
function __autoload($class_name) {
	include BASE_PATH . DS . $class_name . '.php';
}

// Получаем основные параметры настройки подключаения к SOAP
$params = [
	// Ссылка на wsdl ресура
	'wsdl'   => 'http://api.starliner.ru/Api/connect/TrainAPI?wsdl',
	// Ключи для получения данных
	'params' => [
		'auth' => [
			'login'        => 'test',
			'psw'          => 'bYKoDO2it',
			'terminal'     => 'htk_test',
			'represent_id' => 22400,
		],
	]
];

// Вызываем класс вспомогательны утилит
$utils = new \libs\Utils();

// Если приходит AJAX запрос
if ($utils->isAjax()) {

	// Параметры формирования выборки
	$params['params']['train']                = $utils->getParam('train');
	$params['params']['travel_info']['from']  = $utils->getParam('from');
	$params['params']['travel_info']['to']    = $utils->getParam('to');
	$params['params']['travel_info']['day']   = $utils->getParamInt('day');
	$params['params']['travel_info']['month'] = $utils->getParamInt('month');

	// Читаем из wsdl по SOAP протоколу
	$soap = new \libs\WsdlSearch();
	$soap->setParams($params);

	// Отдаем json в ajax
	$utils->jsonResponse([
		'xml' => $soap->getXml(),
	]);

}

/** @var libs\Views $views -- подключаем вьху */
$views = new \libs\Views(VIEW);
$views->getViews('index', [
	'title' => 'Получение расписания',
]);








