<?php

namespace libs;

class Views {

	/**
	 * @var -- путь к директории шаблонов
	 */
	protected $path;

	/**
	 * Получаем путь ко вьюхам
	 * @param $path
	 */
	public function __construct($path) {
		$this->path = $path;
	}

	/**
	 * Получаем вьюху
	 * @param       $template
	 * @param array $data
	 */
	public function getViews($template, array $data = array()) {
		if (is_array($data)) {
			extract($data);
		}
		include_once($this->path . DS . $template . '.php');
	}

}