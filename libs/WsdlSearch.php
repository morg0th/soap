<?php

namespace libs;

class WsdlSearch {

	/**
	 * @var array
	 */
	protected $params = [];

	/**
	 * Месяцы года
	 * @var array
	 */
	protected static $month = [
		1  => 'Январь',
		2  => 'Февраль',
		3  => 'Март',
		4  => 'Апрель',
		5  => 'Май',
		6  => 'Июнь',
		7  => 'Июль',
		8  => 'Август',
		9  => 'Сентябрь',
		10 => 'Октябрь',
		11 => 'Ноябрь',
		12 => 'Декабрь',
	];

	/**
	 * Получаем массив месяцев
	 * или строку месяца по ключу
	 * @param null $key
	 * @return array|string
	 */
	public static function getMonth($key = null) {
		if ($key === null) {
			return self::$month;
		}
		return self::$month[$key] ? self::$month[$key] : '';
	}

	/**
	 * Получаем параметры настройки для SOAP
	 * @param array $params
	 */
	public function setParams(array $params) {
		$this->params = $params;
	}

	/**
	 * Получаем объект SOAP
	 */
	protected function getSoapClient() {
		/** @var \SoapClient */
		return new \SoapClient($this->params['wsdl']);
	}

	/**
	 * Получаем данные из SOAP
	 * @return mixed
	 */
	public function getSoapData() {
		return $this->getSoapClient()->trainRoute(
			$this->params['params']['auth'],
			$this->params['params']['train'],
			$this->params['params']['travel_info']
		);
	}

	/**
	 * Возвращаем шаблон с ответом
	 * в формате XML Для SOAP
	 * @return string
	 */
	public function getXml() {
		// Получаем объект данных
		$xml = new \libs\XmlHelper();
		return $xml->getDomXml($this->getSoapData());
	}

}