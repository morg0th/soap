<?php

namespace libs;

class Utils {

	/**
	 * Проверяем, пришел ли аякс
	 * @return bool
	 */
	public function isAjax() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}

	/**
	 * Проверяем, пришел ли GET
	 * @return bool
	 */
	public function isGet() {
		return $this->getMethod() === 'GET';
	}

	/**
	 * Проверяем, пришел ли POST
	 * @return bool
	 */
	public function isPost() {
		return $this->getMethod() === 'POST';
	}

	/**
	 * Получаем метод данных
	 */
	protected function getMethod() {
		return isset($_SERVER['REQUEST_METHOD']) ? strtoupper($_SERVER['REQUEST_METHOD']) : 'GET';
	}

	/**
	 * Возвращает параметр по ключу, в противном случае дефолтное значение
	 * Приоритет хранилища параметра ($_POST, $_REQUEST etc) определяется настройками сервера (по умолчанию GPC)
	 * @param string     $key
	 * @param mixed|null $default_value
	 * @param array|null $source
	 * @return mixed|null
	 */
	public function getParam($key, $default_value = null, $source = null) {
		if ($source === null) {
			$source = $_REQUEST;
		}
		return isset($source[$key]) && $source[$key] != '' ? $source[$key] : $default_value;
	}

	/**
	 * Получение строки из $_REQUEST.
	 * @param string     $key
	 * @param mixed|null $default_value
	 * @param array|null $source
	 * @return mixed|null
	 */
	public function getParamStr($key, $default_value = null, $source = null) {
		$value = $this->getParam($key, null, $source);
		if (!is_string($value)) {
			return $default_value;
		}
		return $value;
	}

	/**
	 * Получение int значения из $_REQUEST.
	 * Оптимально использовать для получение id записи, тем самым проводя первоначальную валидацию этого значения.
	 * @param string     $key
	 * @param null|int   $default_value
	 * @param array|null $source
	 * @param null|int   $max_value максимальное значение
	 * @return int|null
	 */
	public function getParamInt($key, $default_value = null, $source = null, $max_value = null) {
		$value = $this->getParam($key, null, $source);
		if (!is_numeric($value)) {
			return $default_value;
		}
		$value_int = (int)$this->getParam($key);
		if (is_string($value) && $value !== (string)$value_int) {
			return $default_value;
		}
		if (is_numeric($max_value) && $value_int > $max_value) {
			return $max_value;
		}
		return $value_int;
	}

	/**
	 * Получение float значения из $_REQUEST.
	 * Оптимально использовать для получение значения с плавающей точкой, тем самым проводя первоначальную валидацию этого значения.
	 * @param string     $key
	 * @param null|float $default_value
	 * @param array|null $source
	 * @param null|float $max_value максимальное значение
	 * @return float|null
	 */
	public function getParamFloat($key, $default_value = null, $source = null, $max_value = null) {
		$value = $this->getParam($key, null, $source);
		$value = str_replace(',', '.', $value);
		if (!is_numeric($value)) {
			return $default_value;
		}
		$value_float = $this->getParam($key);
		$value_float = (float)str_replace(',', '.', $value_float);
		if (is_string($value) && $value !== (string)$value_float) {
			return $default_value;
		}
		if (is_numeric($max_value) && $value_float > $max_value) {
			return $max_value;
		}
		return $value_float;
	}

	/**
	 * Получение массива из из $_REQUEST.
	 * @param string     $key
	 * @param mixed|null $default_value
	 * @param array|null $source
	 * @return mixed|null
	 */
	public function getParamArray($key, $default_value = null, $source = null) {
		$value = $this->getParam($key, null, $source);
		if (!is_array($value)) {
			return $default_value;
		}
		return $value;
	}

	/**
	 * Получения массива int значений из запроса
	 * @param string     $key
	 * @param mixed|null $default_value
	 * @param array|null $source
	 * @return mixed|null
	 */
	public function getParamArrayInt($key, $default_value = null, $source = null) {
		$value = $this->getParam($key, null, $source);
		if (!is_array($value)) {
			return $default_value;
		}
		$items = array();
		foreach ($value as $k => $val) {
			$val_int = (int)$val;
			if (!(is_string($val) && $val !== (string)$val_int)) {
				$items[$k] = $val_int;
			}
		}
		return $items;
	}

	/**
	 * Вывод json ответа с очисткой вывода
	 * @param array $data
	 */
	public function jsonResponse(array $data) {
		if (ob_get_length() > 0) {
			ob_end_clean();
		}
		echo json_encode($data);
		exit;
	}

	/**
	 * Форсирование ошибки 404
	 * @param string $message сообщение
	 * @throws \HttpException
	 */
	protected function set404($message = '') {
		throw new \HttpException(404, $message);
	}

}