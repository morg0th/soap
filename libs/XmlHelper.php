<?php

namespace libs;

class XmlHelper {

	/**
	 * Строим xml ответ
	 * @param $soap_obj
	 * @return string
	 */
	public function getDomXml($soap_obj) {

		// Получаем неймспейс для xml
		$ns = 'http://base.google.com/ns/1.0';

		// Объявляем XML
		$xml               = new \DOMDocument('1.0', 'UTF-8');
		$xml->formatOutput = true;

		// Задаем корень дерева документа
		$xml_return = $xml->createElement('return');
		$xml_return->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:ns1', 'http://base.google.com/ns/1.0');

		foreach ($soap_obj as $root_key => $root_val) {
			// Получаем первый дочерний узел
			$xml_root_key = $xml->createElementNS($ns, 'ns1:' . $root_key);

			if ($root_val && is_object($root_val)) {
				foreach ($root_val as $child_key => $child_val) {
					// Если строка, пишем параметры
					if (is_string($child_val) && !is_object($child_val)) {
						$xml_child_key = $xml->createElementNS($ns, 'ns1:' . $child_key, $child_val);
					} else {
						if ($child_key != 'name') {
							$xml_child_key->setAttribute("xsi:nil", 'true');
						}
						$xml_child_key = $xml->createElementNS($ns, 'ns1:' . $child_key);
					}

					if ($child_val && !is_null($child_val) && (is_array($child_val) || is_object($child_val))) {
						foreach ($child_val as $st_key => $st_val) {
							// ПОлучаем второй дочерний узел
							$xml_st_key = $xml->createElementNS($ns, 'ns1:' . $child_key);

							if ($st_val && (is_array($st_val) || is_object($st_val))) {
								foreach ($st_val as $st_k => $st_v) {
									// Получаем третий дочерний узел
									if (is_string($st_v) && !is_object($st_v)) {
										$xml_st_k = $xml->createElementNS($ns, 'ns1:' . $st_k, $st_v);
									} else {
										$xml_st_k = $xml->createElementNS($ns, 'ns1:' . $st_k);
									}
									$xml_st_key->appendChild($xml_st_k);
								}
							}

							$xml_child_key->appendChild($xml_st_key);
						}
					}
					$xml_root_key->appendChild($xml_child_key);
				}
			}
			$xml_return->appendChild($xml_root_key);
		}

		$xml->appendChild($xml_return);

		return $xml->saveXML();
	}
}